const express 		= require('express');
const redis 		= require('redis');
const bodyParser 	= require('body-parser');
const app 			= express();
const Request     	= require("request");

const cors 			= require('cors');
const port 			= 3002;
var async 			= require("async");
app.Request     	= Request;
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended:false}));
app.use(cors())

const http        = require('http');
const server      = http.createServer( app );
const io 		  = require( 'socket.io' )(server);
if( io ){
	app.io = io;
}

let client 	= redis.createClient();

//CADA VEZ QUE EL SERVER SE RE INICIA , GUARDO LAS COORDENAS EN REDIS 
client.on('connect',function(){
	console.log("Todo ok con Redis...")
	client.set('CL', '{"lat":-33.448891,"lng":-70.669266,"name":"Santiago"}');
	client.set('CH', '{"lat":47.376888,"lng":8.541694,"name":"Zurich"}');
	client.set('NZ', '{"lat":-36.848461,"lng":174.763336,"name":"Auckland"}');
	client.set('AU', '{"lat":-33.868820,"lng":151.209290,"name":"Sidney"}');
	client.set('UK', '{"lat":51.507351,"lng":-0.127758,"name":"Londres"}');
	client.set('USA','{"lat":32.165623,"lng":-82.900078,"name":"Georgia"}');
});


/* PARAMETROS PARA CONSUMO DE API WEATHER*/
const weatherApiKey = '9bc78b82d2f20f9c83426641651d0ff8';
const weatherServer = "https://api.darksky.net/forecast/"+weatherApiKey
console.log("WEATHER API ENDPOINT ",weatherServer);


app.io.on( 'connection' , function( socket ){
	console.log("USUARIO CONECTADO",socket.id);
	socket.setMaxListeners(0)
});


/*SERVICIO PARA OBTENER EL CLIMA SEGUN EL ID DEL PAIS */
app.get('/country/:id', function (req, res) {
	try{
		let msg = {
			msg:'ok',
			data:[]
		}
		let id = ''
		if(req.params.id){
			id = req.params.id.toUpperCase()
		}

		client.get( id , function (error, result) {
			if (error) {
				msg.data = [];
				msg.msg = error;
				res.status(400).json(msg);
			}

			if(!result){
				msg.msg="No existen datos para el id "+id
				res.status(400).json(msg);	
			}else{
				let r = JSON.parse( result )
				msg.data = r;
				let reqpam = weatherServer+'/'+r.lat+','+r.lng;
				console.log("LLAMADA A API",reqpam);
				app.Request.get(reqpam, (error, response, body) => {
					if(error) {
						msg.data = [];
						msg.msg = "Hubo un problema al recuperar los datos";
						res.status(400).json(msg);
					}

					msg.msg="ok";
					msg.data = JSON.parse(body);

					res.status(200).json(msg);

				});
			}
		});

	}catch(e){
		msg.msg=e;
		msg.data = [];
		res.status(400).json(msg);
	}
});
/*SERVICIO PARA OBTENER EL CLIMA DE TODOS LOS PAISES */
app.get('/countries', function (req, res) {
	try{

		let msg = {
			msg:'ok',
			data:[]
		}
		if (Math.random(0, 1) < 0.1) throw new Error('How unfortunate! The API Request Failed')

			let id = ''
		if(req.params.id){
			id = req.params.id.toUpperCase()
		}

		client.keys('*', (err, keys) => {
			let wheaters = []
			console.log(keys);
			if( keys ){
				async.map(keys.filter( k => k != 'api.errors'), function(key, cb) {
					
					client.get(key, function (error, value) {
						if(error) {
							return cb(error);
						}
						var w = {};

						let r = JSON.parse( value )
						let reqpam = weatherServer+'/'+r.lat+','+r.lng;
						app.Request.get(reqpam, (error, response, body) => {
							if( response.statusCode != 200 || error || body == 'daily usage limit exceeded' ) {
								return cb(error);
							}else{
								console.log("respuesta request",response.statusCode)
									// console.log("respuesta request",body)
									console.log("respuesta error request",error)
									w['id']=key;
									w['data']=JSON.parse( body  );
									cb(null, w);
								}
							});
					}); 
					
				}, function (error, results) {
					console.log("el error",error);
					if ( error ){
						msg.msg="Hubo un problema al recuperar la data";
						msg.data = [];
						res.status(400).json(msg);
					}else{	
						console.log("resultado",results);
						msg.msg="ok";
						msg.data = results;
						res.status(200).json(msg);
					}
				});
			}
		});

	}catch(e){
		console.log("HUBO UN ERROR",e)
		client.hset('api.errors', Date.now(), e , function(err, reply) {
			if (err) throw err;
		});
		
		res.status(400).json({msg:e});
	}
});
/* SERVICIO PARA LIMPIAR REDIS */
app.get('/clean', function (req, res) {
	client.flushdb( function (err, succeeded) {
		console.log(succeeded); 
		res.send('OK DEL');
	});
});
/* SERVICIO PARA MOSTRAR ERROR REGISTRADOS EN REDIS*/
app.get('/errors', function (req, res) {
	client.hgetall("api.errors", function (err, obj) {
		console.dir(obj);
		let e = {
			"errores":obj
		}
		res.status(200).json(e);
	});
	
});

server.listen( port, function() {
	console.log( 'Up On Port:'+port);

	/* AL INICIARSE EL SERVIDOR SE ESTABLECE UN CONTADOR DE 10 SEGUNDOS PARA EMITIR EVENTO SOCKET DE SINCRONIZACION*/
	setInterval(function(){ 
		// app.io.emit('updateweather');
	}, 10000);

});
